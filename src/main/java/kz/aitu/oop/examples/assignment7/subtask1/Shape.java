package kz.aitu.oop.examples.assignment7.subtask1;

public abstract class Shape {

        protected String color="red";
        protected Boolean filled=true;


        public Shape() {

        }

        public Shape(String color, boolean filled) {
            this.color = color;
            this.filled= filled;

        }

        public String getColor() {
            return this.color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public boolean isFilled(){
            return filled;
        }
        public void setFilled (){
            this.filled=filled;
        }


        public abstract double getArea();

        public abstract double getPerimeter();

    public String toString() {
        return "A Shape with color of "+color+" "+filled;
    }
    }
package kz.aitu.oop.examples.assignment7.subtask1;

public class Circle extends Shape {

        protected double radius=1.0;


    public Circle(){radius=1.0;}

        public Circle(double radius) {

            this.radius = radius;
        }


    public Circle(double radius, String color, boolean filled) {
        super("red",true);
        this.radius = radius;
    };

    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;

    }
    @Override
        public double getArea() {
            return Math.PI * radius * radius;
        }

        public double getPerimeter() {
            return 2.0 * Math.PI * radius;
        }

    @Override
    public String toString() {
        return super.toString()+" "+radius;

}
    }



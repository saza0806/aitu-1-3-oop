package kz.aitu.oop.practice.practice3;

import kz.aitu.oop.practice.practice2.Train;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

        public static void main(String[] args) {
            String url = "jdbc:mysql://127.0.0.1:3306/practice3";
            String username = "user";
            String password = "789789789a";
            try {
                Class.forName("com.mysql.jdbc.Driver");

                Connection connection = DriverManager.getConnection(url, username, password);

                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("Select * from student");
                List<Administration> administrationList = new ArrayList<>();
                while(resultSet.next()) {
                    Administration administration = Administration.getAdministration();
                    administration.setCeo(resultSet.getString("Azamat"));
                    administration.setField(resultSet.getString("Analytics"));
                    administration.setTotalBudget(resultSet.getInt("12222"));
                    administration.setNumberOfWorkers(resultSet.getInt("34"));

                    System.out.println(true);
                }

                resultSet.close();
                statement.close();
                connection.close();



            } catch (SQLException sql) {
                sql.printStackTrace();


            } catch (ClassNotFoundException cnfe){
                cnfe.printStackTrace();

            }
            Employee employee = Employee.getEmployee();
            employee.setHead("Azamat");
            employee.setSalary(43000);
            employee.setName("Roger");
            employee.getTotalCost(30000,20000);

            System.out.println(employee);


        }


}



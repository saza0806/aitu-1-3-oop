package kz.aitu.oop.practice.practice2;

public class Locomotive extends Train {
    private int id;

    private int speed;
    private String engine;
    private int weightOfFreight;

    @Override
    public void setId(int id) {
        this.id = id;
    }
    @Override
    public int getId() {
        return id;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getWeightOfFreight() {
        return weightOfFreight;
    }

    public void setWeightOfFreight(int weightOfFreight) {
        this.weightOfFreight = weightOfFreight;
    }
    public int calculateTotalWeight(int passengersnumber,int numberOfWagons)
    {return passengersnumber+numberOfWagons;
    }
    @Override
    public String toString() {
        return "Locomotive" +
                "Id='" + id + '\'' +
                ", Speed=" + speed +
                ", Engine=" + engine +",Weight of freight"+weightOfFreight+"Total Weight="+calculateTotalWeight(122,223)+
                '}';
    }


}

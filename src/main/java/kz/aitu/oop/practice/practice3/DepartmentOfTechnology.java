package kz.aitu.oop.practice.practice3;

public class DepartmentOfTechnology {
    private String projectManager;
    private String sponsor;
    private int materual;

    public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public int getMaterual() {
        return materual;
    }

    public void setMaterual(int materual) {
        this.materual = materual;
    }
}

package kz.aitu.oop.practice.practice4;
import kz.aitu.oop.practice.practice4.Aquarium;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String url = "jdbc:mysql://127.0.0.1:3306/practice4";
        String username = "user";
        String password = "789789789a";
        try {
            Class.forName("com.mysql.jdbc.Driver");

            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from student");
            List<Aquarium> aquariumList = new ArrayList<>();
            while(resultSet.next()) {
                Aquarium aquarium = new Aquarium();
                aquarium.setId(resultSet.getInt("1"));
                aquarium.setName(resultSet.getString("jellyfish"));
                aquarium.setOwnerName(resultSet.getString("Michael"));
                aquarium.setAddress(resultSet.getString("Brazil,Rio"));
                aquarium.setAnimalType(resultSet.getString("fish"));

                System.out.println(true);
            }

            resultSet.close();
            statement.close();
            connection.close();



        } catch (SQLException sql) {
            sql.printStackTrace();


        } catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();

        }
        Fish fish = Fish.getFish();
        fish.setLifeLength(24);
        fish.setOrigin("Brazil");
        fish.setPrice(4400);
        fish.getTotalCost(440,33);


        System.out.println(fish);


    }


}
package kz.aitu.oop.practice.practice1;

public class Person {
    private int height;
    private String name;
    private int weight;
    public static boolean fileExists(String str){return true;}
    public static boolean isInt(String  str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }


    }
    public static boolean isDouble(String str){
        double val=Double.parseDouble(str);
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}

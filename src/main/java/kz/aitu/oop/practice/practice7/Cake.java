package kz.aitu.oop.practice.practice7;

public class Cake implements Food {
    public String type;
    public String getType(){return this.type;}
    @Override
    public String toString(){
        return "Factory returned a Class Cake." +
                "Someone ordered a dessert!";
}


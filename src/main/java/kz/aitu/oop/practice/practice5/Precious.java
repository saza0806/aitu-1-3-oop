package kz.aitu.oop.practice.practice5;

public class Precious implements Stone {
    private int id;
    private String name;
    private int price;
    private String origin;
    private int numberOfP;
    private static Precious preciousinstance=null;
    private Precious(){}
public static Precious getPrecious(){
        if (preciousinstance==null)
            preciousinstance=new Precious();
        return preciousinstance;

}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getNumberOfP() {
        return numberOfP;
    }

    public void setNumberOfP(int numberOfP) {
        this.numberOfP = numberOfP;
    }
    public int getTotalWeight(int numberOfP, int price)
    {
        return price-(numberOfP*3);
    }
}

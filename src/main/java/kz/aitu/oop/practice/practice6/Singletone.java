package kz.aitu.oop.practice.practice6;

public class Singletone {
    public String str;
    private static Singletone singletoneinstance=null;
    private Singletone(){}
    public static Singletone getSingle(){
        if (singletoneinstance==null)
            singletoneinstance=new Singletone();
        return singletoneinstance;
    }

    @Override
    public String toString() {
        return "Singletone{" +"Hello, I'm singletone! Let me say "
                + str +" to you"+
                '}';
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}

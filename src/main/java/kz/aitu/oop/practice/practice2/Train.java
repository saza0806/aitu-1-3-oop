package kz.aitu.oop.practice.practice2;


public class Train {
    private int id;

    private String color;
    private int numberOfWagons;
    private int passengersnumber;
    private String driver_name;




    public String toString() {
        return "Train{" +
                "color='" + color + '\'' +
                ", number of wagons=" + numberOfWagons +
                ", passenger's number=" + passengersnumber +",Name of the driver"+driver_name+
                '}';
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }



    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNumberOfWagons() {
        return numberOfWagons;
    }

    public void setNumberOfWagons(int numberOfWagons) {
        this.numberOfWagons = numberOfWagons;
    }

    public int getPassengersnumber() {
        return passengersnumber;
    }

    public void setPassengersnumber(int passengersnumber) {
        this.passengersnumber = passengersnumber;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

}

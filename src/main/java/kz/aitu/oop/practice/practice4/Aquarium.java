package kz.aitu.oop.practice.practice4;

public class Aquarium {
    private int id;
    private String name;
    private String ownerName;
    private String address;
    private String animalType;
    private static Aquarium aquariuminstance = null;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public String toString() {
        return "Aquarium{" +
                "ID='" + id + '\'' +
                ", number of the animal=" + name +
                ", owner's number=" + ownerName + ",Address" + address + ",Type of the animal:" + animalType +
                '}';
    }
}


package kz.aitu.oop.examples.assignment5;

public class Square extends Rectangle
{
    protected double side=1.0;


    public Square(){

        side = 1.0;}

    public Square (double side) {



        this.side = side;

    };
    public Square(double side, String color, boolean filled)
    {
        super(color, filled);
        this.side = side;

    }
    public double getSide(){
        return side;
    }
    public void setSide(double side){
        this.side = side;
    }

    public void setWidth(double side){
        this.side = side;
    }

    public void setLength(double side){
        this.side = side;
    }

    @Override
    public String toString(){
        return "A square with side "+side+" , which is a subclass of "+super.toString();
    }






    }


package kz.aitu.oop.practice.practice2;

public class Car extends Train {

    private int id;
    private int speed;

    private int year;
    private String engine;
    private static  Car carinstance=null;
    private Car(){}
    public static Car getCar(){
        if (carinstance==null)
            carinstance=new Car();
        return carinstance;

    }




    @Override
    public void setId(int id) {
        this.id = id;
    }
    @Override
    public int getId() {
        return id;
    }
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
    public int calculateDurationofTrip(int year, int speed)
    {
        return (int) (year*0.5*speed);


    }

    @Override
    public String toString() {
        return "Car" +
                "Id='" + id + '\'' +
                ", Speed=" + speed +
                ", Year=" + year +",Name of the engine"+engine+
                '}';
    }

}

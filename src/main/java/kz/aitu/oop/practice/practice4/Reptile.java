package kz.aitu.oop.practice.practice4;

public class Reptile {
    private String environment;
    private int price;
    private String animalClass;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getAnimalClass() {
        return animalClass;
    }

    public void setAnimalClass(String animalClass) {
        this.animalClass = animalClass;
    }
    @Override
    public String toString() {
        return "Reptiale" +
                "Environment='" + environment + '\'' +
                ",Price="+price+", Animal class=" + animalClass +
                '}';
    }
}

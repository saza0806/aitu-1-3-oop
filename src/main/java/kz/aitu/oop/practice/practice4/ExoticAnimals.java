package kz.aitu.oop.practice.practice4;

public class ExoticAnimals {
    private String environment;
    private int livingCycle;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public int getLivingCycle() {
        return livingCycle;
    }


    public void setLivingCycle(int livingCycle) {
        this.livingCycle = livingCycle;
    }
    @Override
    public String toString() {
        return "Exotic animal" +
                "Environment='" + environment + '\'' +
                ", Living cycle=" + livingCycle +
                                '}';
    }
}

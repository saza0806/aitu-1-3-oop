package kz.aitu.oop.examples.assignment7.subtask1;

import kz.aitu.oop.examples.assignment7.subtask1.Rectangle;

public class Square extends Rectangle {
    private double side= 1.0;;

    public Square(){
        this.side=1.0;
    }
     public Square (double side) {
         super(side);
         this.side = side;

    };

    public Square(double side, String color, boolean filled)
    {
        super(color, filled);
        this.side = side;

    }
    public double getSide(){
        return side;
    }
    public void setSide(double side){
        this.side = side;
    }

    public void setWidth(double side){
        this.side = side;
    }

    public void setLength(double side){
        this.side = side;
    }

    @Override
    public String toString(){
        return "A square with side "+side+" , which is a subclass of "+super.toString();
    }
    @Override
    public double getArea (){
        return getLength()*getLength();
    }
    @Override
    public double getPerimeter (){
        return 2*(getLength()+getLength());
    }

    public void setWidth(){
        super.setWidth( side);
    }



    public void setLength(){
        super.setLength( side);
    }

}

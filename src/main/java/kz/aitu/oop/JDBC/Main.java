package kz.aitu.oop.JDBC;
import kz.aitu.oop.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;



public class Main {
    public static void main(String[] args) {
        String url = "jdbc:mysql://127.0.0.1:3306/student";
        String username = "user2";
        String password = "123";
        try {
            Class.forName("com.mysql.jdbc.Driver");

            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from student");
            List<Student> studentList = new ArrayList<>();
            while(resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setFirst_name(resultSet.getString("first_name"));
                student.setLast_name(resultSet.getString("Last_name"));
                student.setSpecialty(resultSet.getString("Specialty"));
                student.setGroup(resultSet.getString("Group"));

                System.out.println(student);
            }

            resultSet.close();
            statement.close();
            connection.close();



        } catch (SQLException sql) {
            sql.printStackTrace();


        } catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();

        }
    }
    }
/*
        private String first_name;
        private String last_name;
        private int ID;
        private  String Group;
        private String Specialty;

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getGroup() {
            return Group;
        }

        public void setGroup(String group) {
            Group = group;
        }

        public String getSpecialty() {
            return Specialty;
        }

        public void setSpecialty(String specialty) {
            Specialty = specialty;
        }

    }
}
*/
package kz.aitu.oop.examples.assignment7.subtask2;

public interface Movable {
    void MoveUp();
    void MoveDown();
    void MoveLeft();
    void MoveRight();
}

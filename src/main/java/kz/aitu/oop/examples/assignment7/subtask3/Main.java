package kz.aitu.oop.examples.assignment7.subtask3;

public class Main {
    public static void main(String[] args) {
        GeometricObject geomObj1 = new Circle(5.0);
        System.out.println(geomObj1);
        System.out.println("Perimeter = " + geomObj1.getPerimeter());
        System.out.println("Area = " + geomObj1.getArea());
    }
}

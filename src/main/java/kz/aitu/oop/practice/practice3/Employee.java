package kz.aitu.oop.practice.practice3;

public class Employee {
    private String head;
    private int salary;
    private String name;

    private static Employee employeeinstance = null;
    private Employee(){}
    public static Employee getEmployee(){
        if (employeeinstance==null)
            employeeinstance=new Employee();
        return employeeinstance;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getTotalCost(int salary,int materials){
        return salary-(materials*3);

    }
    @Override
    public String toString() {
        return "Employee" +
                "head of the employee='" + head + '\'' +
                ", Salary=" + salary +
                ", name of the Employee=" + name +
                '}';
    }
}

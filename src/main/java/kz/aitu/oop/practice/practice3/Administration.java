package kz.aitu.oop.practice.practice3;

public class Administration {
    private String ceo;
    private String field;
    private int totalBudget;
    private int numberOfWorkers;
    private static Administration administrationinstance=null;
    public Administration(){}
    public static Administration getAdministration(){
        if (administrationinstance==null)
            administrationinstance=new Administration();
        return administrationinstance;

    }

    public String toString() {
        return "Administration{" +
                "CEO='" + ceo + '\'' +
                ", Total budget of inc.=" + totalBudget +
                ", Number of workers=" + numberOfWorkers +",Name of the field"+field+
                '}';
    }


    public String getCeo() {
        return ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(int totalBudget) {
        this.totalBudget = totalBudget;
    }

    public int getNumberOfWorkers() {
        return numberOfWorkers;
    }

    public void setNumberOfWorkers(int numberOfWorkers) {
        this.numberOfWorkers = numberOfWorkers;
    }
}

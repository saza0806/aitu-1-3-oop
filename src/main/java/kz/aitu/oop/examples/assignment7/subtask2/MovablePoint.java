package kz.aitu.oop.examples.assignment7.subtask2;

public class MovablePoint implements Movable {
     int x;
     int y;
    int xSPeed;
    int ySpeed;



    public MovablePoint(int x, int y, int xSPeed, int ySpeed) {

        this.x = x;
        this.y = y;
        this.xSPeed = xSPeed;
        this.ySpeed = ySpeed;
    }


    @Override
    public String toString() {
        return "MovablePoint [x=" + x + ", y=" + y + ", xSPeed=" + xSPeed + ", ySpeed=" + ySpeed + "]";
    }


    public void MoveLeft() {


    }


    public void MoveRight() {

    }

    public void MoveUp() {


    }


    public void MoveDown() {



    }



}

package kz.aitu.oop.practice.practice5;

public class SemiPrecious {
    private String name;
    private int visitorsNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVisitorsNumber() {
        return visitorsNumber;
    }

    public void setVisitorsNumber(int visitorsNumber) {
        this.visitorsNumber = visitorsNumber;
    }
    @Override
    public String toString() {
        return "Semipricious" +
                "name='" + name + '\'' +
                ",number of visitors="+visitorsNumber +
                '}';
    }
}

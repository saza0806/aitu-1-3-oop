package kz.aitu.oop.practice.practice4;

public class Fish {
    private int lifeLength;
    private String origin;
    private int price;
    private static Fish fishinstance=null;
    private Fish (){}
    public static Fish getFish(){
        if (fishinstance==null)
            fishinstance=new Fish();
        return fishinstance;

    }

    public int getLifeLength() {
        return lifeLength;
    }

    public void setLifeLength(int lifeLength) {
        this.lifeLength = lifeLength;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public int getTotalCost(int price,int lifeLength){
        return (lifeLength*34)-price;

    }
    @Override
    public String toString() {
        return "Fish" +
                "Lifelength='" + lifeLength + '\'' +
                ", Origin of the fish=" + origin +
                ", Price of the fish=" + price +"Total cost="+getTotalCost(3330,33)+
                '}';
    }
}
